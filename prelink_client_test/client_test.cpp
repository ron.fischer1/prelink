#include "../libprelink/prelink_client.h"
#include "../libprelink/lookat.h"
#include <iostream>

#define PORT 4444

class WS2 {
public:
	static bool Initialize();
	static bool Shutdown();
};

bool WS2::Initialize()
{
	WSADATA wsaData;
	int error = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (error)
	{
		std::cerr << "could not start winsock " << WSAGetLastError() << std::endl;
		return false;

	}

	if (wsaData.wVersion != MAKEWORD(2, 2)) // Wrong Winsock version?
	{
		WSACleanup();
		return false;
	}

	return true;
}

bool WS2::Shutdown()
{
	return WSACleanup() != SOCKET_ERROR;
}

#if 0

// Incomplete code. Intended for keypress to disconnect and reconnect, etc.

#include <Winsock2.h>

bool keypressed()
{
	fd_set read_fds;
	FD_ZERO(&read_fds);
	FD_SET(STDIN_FILENO, &read_fds);

	int result = select(NULL, &read_fds, NULL, NULL, NULL);
	if (result == -1 && errno != EINTR)
	{
		std::cerr << "Error in select: " << strerror(errno) << "\n";
		return false;
	}
	else if (result == -1 && errno == EINTR)
	{
		//we've received and interrupt - handle this
		// KICK!
	}
	else
	{
		if (FD_ISSET(STDIN_FILENO, &read_fds))
		{
			process_cmd(sfd);
		}
	}
}

#endif

int main(int argc, char* argv[])
{
	//char* server = "192.168.1.11";
	char* server = "127.0.0.1";	// if testing on local machine, cannot be a name
	unsigned short port = PORT;

	if (argc == 1)
		;
	else if (argc == 2)
	{
		server = argv[1];
	}
	else
	{
		std::cerr << "Usage: " << argv[0] << " [<server>]" << std::endl;
		exit(-1);
	}

	if (!WS2::Initialize())
		return -1;
	std::cout << "WS2 initialized\n";

	PrelinkClient* plc = new PrelinkClient();
	plc->SetNetworkAddress(server);
	plc->SetNetworkPort(port);
	if (!plc->Open())
	{
		std::cerr << "Could not open server connection" << std::endl;
		exit(-1);
	}

	std::cout << "Opened " << server << ":" << port << std::endl;

	plc->Receive(50);	// Previzion quirk: must wait one frame time before request (slowest 24fps around 41.6ms)

	// send status request
	if (!plc->StreamCamera(true))
	{
		std::cerr << "Could not request camera status stream" << std::endl;
		exit(-1);
	}

	std::cout << "Requested camera status stream" << std::endl;

	plc->mCounter = 0;

	// Used to delay camera status stream request
	int samples = 0;
	bool requested = false;
	
	do
	{
		if (plc->Receive(1000 / 60))
		{
			std::cout << plc->mTimecode;
			std::cout.precision(4);
			std::cout << " " << plc->mStatus.position[0] << " " << plc->mStatus.position[1] << " " << plc->mStatus.position[2];
			std::cout << " " << plc->mStatus.forward[0] << " " << plc->mStatus.forward[1] << " " << plc->mStatus.forward[2];
			double rotation[3];
			LookAt(rotation, plc->mStatus.up, plc->mStatus.forward);
			std::cout << " " << rotation[0] << " " << rotation[1] << " " << rotation[2];
			std::cout << "           \r";
		}

		// Wait 1 second before request camera status stream
		if (false && !requested && samples > 60)
		{
			plc->StreamCamera(true);
			requested = true;
			std::cout << "Requested camera status stream" << std::endl;
		}

		samples++;
	} while (plc->IsConnected());

	if (!plc->Close()) {
		std::cerr << "Close failed" << std::endl;
		exit(-1);
	}

	std::cout << "Closed" << std::endl;
	
	return 0;
}
