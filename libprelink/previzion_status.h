#ifndef __PREVIZION_CAMERA_FRAME_H__
#define __PREVIZION_CAMERA_FRAME_H__

#include <vector>
#include "bitflix\bitflix_time.h"
#define _USE_MATH_DEFINES
#include <math.h>

// The format specified by Phil is capable of more generality
// In particular, every status record sent from Previzion could potentially contain:
// 1) a list of non-time related statii
// 2) a list of frame times, each containing:
//   2a) a list of unordered time related statii

#ifdef _USRDLL
        // .dll
        #define DECLSPECIFIER __declspec(dllexport)
        #define EXPIMP_TEMPLATE
#else
        // .exe
        #define DECLSPECIFIER __declspec(dllimport)
        #define EXPIMP_TEMPLATE extern
#endif

class DECLSPECIFIER PrevizionStatus {
public:
	virtual void dummy() {}
};

EXPIMP_TEMPLATE template class DECLSPECIFIER std::vector<PrevizionStatus*>;

class DECLSPECIFIER CameraStatus : public PrevizionStatus {
public:
	CameraStatus() : stereo(false) {}
	double position[3], forward[3], up[3];
	double xfov, focus, iris, subject, distortion, interocular, convergence;
	bool stereo;

	CameraStatus& operator=(CameraStatus const &other) {
		for(int i = 0; i < 3; i++) {
			position[i] = other.position[i];
			forward[i] = other.forward[i];
			up[i] = other.up[i];
		}
		xfov = other.xfov;
		focus = other.focus;
		iris = other.iris;
		subject = other.subject;
		distortion = other.distortion;
		interocular = other.interocular;
		convergence = other.convergence;
		stereo = other.stereo;
		return *this;
	}

	// return value in degrees
	static double getYFoV(double xfov) {
		double lSensorWidth = 16.0, lSensorHeight = 9.0;	// Previzion only handles 16:9
		double xfovr = xfov / 180.0 * M_PI;
		double yfovr = 2.0 * atan((lSensorHeight * tan(xfovr / 2.0)) / lSensorWidth);
		return yfovr * 180.0 / M_PI;
	}

	// xsensor in mm
	static double getFocalLength(double xsensor, double xfovdegrees) {
		double xfovr = xfovdegrees / 180.0  * M_PI;
		return xsensor /  (2.0 * tan(xfovr / 2.0) );
	}
};

class DECLSPECIFIER PrevizionFrame {
public:
	PrevizionFrame() : mHasTime(false), mTime(0, 0, 0, 0, 0.0) {}
	PrevizionFrame(BFTimecode &tc) : mHasTime(true), mTime(tc) {}
	bool mHasTime;
	BFTimecode mTime;
	
	std::vector<PrevizionStatus*> mStates;
};

typedef std::vector<PrevizionFrame*> PrevizionUpdate;

#endif
