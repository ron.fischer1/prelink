#include "lookat.h"

#include "../gmtl/gmtl.h"

void Compose(gmtl::Matrix33d &matrix, gmtl::Vec3d right, gmtl::Vec3d up, gmtl::Vec3d forward)
{
	gmtl::identity(matrix);

	matrix(0, 0) = right[0];
	matrix(0, 1) = right[1];
	matrix(0, 2) = right[2];

	matrix(1, 0) = up[0];
	matrix(1, 1) = up[1];
	matrix(1, 2) = up[2];

	matrix(2, 0) = forward[0];
	matrix(2, 1) = forward[1];
	matrix(2, 2) = forward[2];
}

// Previzion: looks down x+ with z+ pointing right
// Motionbuilder: follows Previzion
// Maya: looks down z- with x+ pointing right
void LookAt(double pRotation[3], double pUp[3], double pForward[3])
{
	gmtl::Vec3d up(pUp[0], pUp[1], pUp[2]), x(pForward[0], pForward[1], pForward[2]);

	gmtl::normalize(x);

	gmtl::Vec3d z;
	gmtl::cross(z, up, x);
	gmtl::normalize(z);

	gmtl::Vec3d y;
	gmtl::cross(y, z, x);

	gmtl::Matrix33d lMatrix;
	Compose(lMatrix, z, y, x);

	gmtl::EulerAngleXYZd lRotation;
	lRotation = gmtl::makeRot< gmtl::EulerAngleXYZd >( lMatrix );

	pRotation[0] = lRotation[0];
	pRotation[1] = lRotation[1];
	pRotation[2] = lRotation[2];
}
