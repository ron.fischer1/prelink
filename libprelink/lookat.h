#ifndef __LOOKAT_H__
#define __LOOKAT_H__

__declspec(dllexport) void LookAt(double pRotation[3], double pUp[3], double pForward[3]);

#endif __LOOKAT_H__
