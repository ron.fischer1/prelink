//-
// ==========================================================================
// Copyright 2014 Bitflix LLC. All rights reserved.
// ==========================================================================
//+

// Description:
// Receives live tracked camera information from Previzion
//

#include "libprelink/libprelink.h"
#include "libprelink/lookat.h"

#include <maya/MFnPlugin.h>
#include <maya/MTypeId.h>

#include <api_macros.h>
#include <maya/MIOStream.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MPxClientDeviceNode.h>

//

#define	PLUGIN_VENDOR "bitflix"
#define PLUGIN_VERSION "1.0.0"


class precamNode : public MPxClientDeviceNode
{

public:
						precamNode();
	virtual 			~precamNode();
	
	virtual void		postConstructor();
	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	virtual void		threadHandler( const char* serverName, const char* deviceName );
	virtual void		threadShutdownHandler();

	static void*		creator();
	static MStatus		initialize();

public:

	static MObject		outputTranslate;
	static MObject 		outputTranslateX;
	static MObject		outputTranslateY;
	static MObject 		outputTranslateZ;

	static MObject		outputRotate;
	static MObject 		outputRotateX;
	static MObject		outputRotateY;
	static MObject 		outputRotateZ;

	static MObject		inputHorizontalFilmAperture;
	static MObject		outputHFoV;
	static MObject		outputFocalLength;
	static MObject		outputVerticalFilmAperture;

	static MObject		outputFocus;
	static MObject		outputIris;
	static MObject		outputSubject;
	static MObject		outputDistortion;
	static MObject		outputInteraxial;
	static MObject		outputConvergence;

	static MObject		outputTCHour;
	static MObject		outputTCMinute;
	static MObject		outputTCSecond;
	static MObject		outputTCFrame;

	static MTypeId		id;

private:

	static PrelinkClient precamClient;
};

MTypeId precamNode::id( 0x00080AF8 );
MObject precamNode::outputTranslate;
MObject precamNode::outputTranslateX;
MObject precamNode::outputTranslateY;
MObject precamNode::outputTranslateZ;
MObject precamNode::outputRotate;
MObject precamNode::outputRotateX;
MObject precamNode::outputRotateY;
MObject precamNode::outputRotateZ;
MObject precamNode::outputHFoV;
MObject precamNode::outputFocus;
MObject precamNode::outputIris;
MObject precamNode::outputSubject;
MObject precamNode::outputDistortion;
MObject precamNode::outputInteraxial;
MObject precamNode::outputConvergence;
MObject precamNode::outputTCHour;
MObject precamNode::outputTCMinute;
MObject precamNode::outputTCSecond;
MObject precamNode::outputTCFrame;

MObject precamNode::inputHorizontalFilmAperture;
MObject precamNode::outputFocalLength;
MObject precamNode::outputVerticalFilmAperture;

PrelinkClient precamNode::precamClient;

precamNode::precamNode()
{}

precamNode::~precamNode()
{
	destroyMemoryPools();
}

void precamNode::postConstructor()
{
	MObjectArray attrArray;

	attrArray.append(precamNode::outputTranslate);
	attrArray.append(precamNode::outputRotate);
	attrArray.append(precamNode::outputHFoV);
	attrArray.append(precamNode::outputFocus);
	attrArray.append(precamNode::outputIris);
	attrArray.append(precamNode::outputSubject);
	attrArray.append(precamNode::outputDistortion);
	attrArray.append(precamNode::outputInteraxial);
	attrArray.append(precamNode::outputConvergence);
	attrArray.append(precamNode::outputTCHour);
	attrArray.append(precamNode::outputTCMinute);
	attrArray.append(precamNode::outputTCSecond);
	attrArray.append(precamNode::outputTCFrame);

	attrArray.append(precamNode::inputHorizontalFilmAperture);
	attrArray.append(precamNode::outputFocalLength);
	attrArray.append(precamNode::outputVerticalFilmAperture);

	setRefreshOutputAttributes(attrArray);

	createMemoryPools( 4, 17, sizeof(double));
}

static unsigned int threadReceipts = 0;

void precamNode::threadHandler(const char* serverName, const char* deviceName)
{
	MStatus status;

	//cerr << "precamNode::threadHandler called " << serverName << " " << deviceName << endl;

	// thread is restarted whenever parameters change (presumably old thread is killed?)
	if (precamClient.IsConnected())
		precamClient.Close();

	// open server connection
	precamClient.SetNetworkAddress(serverName);	// XXX parse from serverName?
	precamClient.SetNetworkPort(DEFAULT_PRELINK_PORT);	// XXX parse from serverName?
	if (!precamClient.Open())
	{
		cerr << "precamNode::threadHandler could not open: " << serverName << ", " << deviceName << endl;
		return;
	}

	// Previzion ignores all request packets within 1 frame time of connection open
	// Following waits 2x longest frame time (~42msec)
	precamClient.Receive((1000 / 24) * 2);

	if (!precamClient.StreamCamera(true))
	{
		cerr << "precamNode::threadHandler could not enable camera stream\n";
		return;
	}

	//cerr << "precamNode::threadHandler entering loop\n";

	setDone(false);
	while (!isDone())
	{
		// block max 1 second for next packet
		//cerr << "precamNode: receiving...\n";
		if (!precamClient.Receive(1000UL))
			break;
		threadReceipts++;
		//cerr << "precamNode: receipt #" << threadReceipts << endl;

		if (!isLive())
			continue;

		// prep a thread to maya data transfer buffer
		MCharBuffer buffer;
		status = acquireDataStorage(buffer);
		if (!status)
			continue;

		// push data into the buffer, bracketed for thread safety
		beginThreadLoop();
		{
			double* doubleData = reinterpret_cast<double*>(buffer.ptr());

			doubleData[0] = precamClient.mStatus.position[0];
			doubleData[1] = precamClient.mStatus.position[1];
			doubleData[2] = precamClient.mStatus.position[2];

			double rotation[3];
			LookAt(rotation, precamClient.mStatus.up, precamClient.mStatus.forward);
			doubleData[3] = rotation[0] * 180.0f / M_PI;
			doubleData[4] = 180.0 - rotation[1] * 180.0f / M_PI;
			doubleData[5] = rotation[2] * 180.0f / M_PI;

			doubleData[6] = precamClient.mStatus.xfov;
			doubleData[7] = precamClient.mStatus.focus;
			doubleData[8] = precamClient.mStatus.iris;
			doubleData[9] = precamClient.mStatus.subject;
			doubleData[10] = precamClient.mStatus.distortion;
			doubleData[11] = precamClient.mStatus.interocular;
			doubleData[12] = precamClient.mStatus.convergence;
			doubleData[13] = precamClient.mTimecode.hour;
			doubleData[14] = precamClient.mTimecode.minute;
			doubleData[15] = precamClient.mTimecode.second;
			doubleData[16] = precamClient.mTimecode.frame;
			
			pushThreadData(buffer);
			//cerr << "precamNode::threadHandler pushed, buffer count" << threadDataCount() << endl;
		}
		endThreadLoop();
	}
	//cerr << "precamNode::threadHandler fell out of loop, closing..,\n";
	if (precamClient.Close())
		;//cerr << "precamNode: closed\n";
	else
		cerr << "precamNode::threadHandler could not close\n";

	setDone(true);
}

void precamNode::threadShutdownHandler()
{
	//cerr << "precamNode::threadShutdownHandler\n";
	setDone( true );
}

void* precamNode::creator()
{
	return new precamNode;
}

#define Q(x) #x
#define QUOTE(x) Q(x)

#define BUILDATTR(NAME,ABBR) NAME = numAttr.create(QUOTE(NAME), QUOTE(ABBR), MFnNumericData::kDouble, 0.0, &status); MCHECKERROR(status, "create "##QUOTE(NAME))
#define BUILDATTR1(NAME,ABBR) NAME = numAttr.create(QUOTE(NAME), QUOTE(ABBR), MFnNumericData::kDouble, 1.0, &status); MCHECKERROR(status, "create "##QUOTE(NAME))


MStatus precamNode::initialize()
{
	MStatus status;
	MFnNumericAttribute numAttr;

	BUILDATTR(outputTranslateX, otx);
	BUILDATTR(outputTranslateY, oty);
	BUILDATTR(outputTranslateZ, otz);
	outputTranslate = numAttr.create("outputTranslate", "ot",
		outputTranslateX, outputTranslateY, outputTranslateZ, &status);
	MCHECKERROR(status, "create outputTranslate");

	BUILDATTR(outputRotateX, orx);
	BUILDATTR(outputRotateY, ory);
	BUILDATTR(outputRotateZ, orz);
	outputRotate = numAttr.create("outputRotate", "oro",
		outputRotateX, outputRotateY, outputRotateZ, &status);
	MCHECKERROR(status, "create outputRotate");

	BUILDATTR1(outputHFoV, ohfov);

	BUILDATTR(outputFocus, ofc);
	BUILDATTR(outputIris, oir);
	BUILDATTR(outputSubject, osj);
	BUILDATTR(outputDistortion, ods);
	BUILDATTR(outputInteraxial, oia);
	BUILDATTR(outputConvergence, ocn);
	BUILDATTR(outputTCHour, otch);
	BUILDATTR(outputTCMinute, otcm);
	BUILDATTR(outputTCSecond, otcs);
	BUILDATTR(outputTCFrame, otcf);

	ADD_ATTRIBUTE(outputTranslate);
	ADD_ATTRIBUTE(outputRotate);
	ADD_ATTRIBUTE(outputHFoV);
	ADD_ATTRIBUTE(outputFocus);
	ADD_ATTRIBUTE(outputIris);
	ADD_ATTRIBUTE(outputSubject);
	ADD_ATTRIBUTE(outputDistortion);
	ADD_ATTRIBUTE(outputInteraxial);
	ADD_ATTRIBUTE(outputConvergence);
	ADD_ATTRIBUTE(outputTCHour);
	ADD_ATTRIBUTE(outputTCMinute);
	ADD_ATTRIBUTE(outputTCSecond);
	ADD_ATTRIBUTE(outputTCFrame);

	// Calculated output from input
	BUILDATTR1(inputHorizontalFilmAperture, ihfa);
	ADD_ATTRIBUTE(inputHorizontalFilmAperture);

	BUILDATTR1(outputFocalLength, ofl);
	ADD_ATTRIBUTE(outputFocalLength);
	//CHECK_MSTATUS(numAttr.setStorable(false));

	BUILDATTR1(outputVerticalFilmAperture, ovfa);
	ADD_ATTRIBUTE(outputVerticalFilmAperture);

	// thread signals change by setting output dirty
	ATTRIBUTE_AFFECTS(output, outputTranslate);
	ATTRIBUTE_AFFECTS(output, outputRotate);
	ATTRIBUTE_AFFECTS(output, outputHFoV);
	ATTRIBUTE_AFFECTS(output, outputFocus);
	ATTRIBUTE_AFFECTS(output, outputIris);
	ATTRIBUTE_AFFECTS(output, outputSubject);
	ATTRIBUTE_AFFECTS(output, outputDistortion);
	ATTRIBUTE_AFFECTS(output, outputInteraxial);
	ATTRIBUTE_AFFECTS(output, outputConvergence);
	ATTRIBUTE_AFFECTS(output, outputTCHour);
	ATTRIBUTE_AFFECTS(output, outputTCMinute);
	ATTRIBUTE_AFFECTS(output, outputTCSecond);
	ATTRIBUTE_AFFECTS(output, outputTCFrame);

	ATTRIBUTE_AFFECTS(output, outputFocalLength);
	ATTRIBUTE_AFFECTS(output, outputVerticalFilmAperture);

	// Following are neccessary
	ATTRIBUTE_AFFECTS(live, outputTranslate);
	ATTRIBUTE_AFFECTS(frameRate, outputTranslate);
	ATTRIBUTE_AFFECTS(serverName, outputTranslate);
	ATTRIBUTE_AFFECTS(deviceName, outputTranslate);

	ATTRIBUTE_AFFECTS(live, outputRotate);
	ATTRIBUTE_AFFECTS(frameRate, outputRotate);
	ATTRIBUTE_AFFECTS(serverName, outputRotate);
	ATTRIBUTE_AFFECTS(deviceName, outputRotate);

	// for the pass-through calculation
	ATTRIBUTE_AFFECTS(inputHorizontalFilmAperture, outputFocalLength);
	ATTRIBUTE_AFFECTS(inputHorizontalFilmAperture, outputVerticalFilmAperture);

	return MS::kSuccess;
}

#define BUILDINHANDLE(NAME) MDataHandle NAME##Handle = block.inputValue(NAME, &status);MCHECKERROR(status,"precamNode::compute error in block.inputValue for "##QUOTE(NAME));double& NAME = NAME##Handle.asDouble()

#define BUILDHANDLE(NAME) MDataHandle NAME##Handle = block.outputValue(NAME, &status);MCHECKERROR(status,"precamNode::compute error in block.outputValue for "##QUOTE(NAME));double& NAME = NAME##Handle.asDouble()

MStatus precamNode::compute(const MPlug& plug, MDataBlock& block)
{
	MStatus status;
	if( plug == outputTranslate || 
		plug == outputTranslateX || plug == outputTranslateY || plug == outputTranslateZ ||
		plug == outputRotate || 
		plug == outputRotateX || plug == outputRotateY || plug == outputRotateZ ||
		plug == outputHFoV ||
		plug == outputFocus ||
		plug == outputIris ||
		plug == outputSubject ||
		plug == outputDistortion ||
		plug == outputInteraxial ||
		plug == outputConvergence ||
		plug == outputTCHour ||
		plug == outputTCMinute ||
		plug == outputTCSecond ||
		plug == outputTCFrame ||
		plug == outputVerticalFilmAperture ||
		plug == outputFocalLength )
	{
		// Access the data and update the output attribute
		MCharBuffer buffer;
		if ( popThreadData(buffer) )
		{
			// Relative data coming in
			double* doubleData = reinterpret_cast<double*>(buffer.ptr());

			MDataHandle outputTranslateHandle = block.outputValue( outputTranslate, &status );
			MCHECKERROR(status, "precamNode::compute Error in block.outputValue for outputTranslate");

			double3& outputTranslate = outputTranslateHandle.asDouble3();
			outputTranslate[0] = doubleData[0];
			outputTranslate[1] = doubleData[1];
			outputTranslate[2] = doubleData[2];

			MDataHandle outputRotateHandle = block.outputValue(outputRotate, &status);
			MCHECKERROR(status, "precamNode::compute Error in block.outputValue for outputRotate");

			double3& outputRotate = outputRotateHandle.asDouble3();
			outputRotate[0] = doubleData[3];
			outputRotate[1] = doubleData[4];
			outputRotate[2] = doubleData[5];

			BUILDHANDLE(outputHFoV);
			double hfov = doubleData[6];
			outputHFoV = hfov > 1.0 ? hfov : 1.0; // = precamClient.mStatus.xfov;

			BUILDHANDLE(outputFocus);
			outputFocus = doubleData[7]; // = precamClient.mStatus.focus;

			BUILDHANDLE(outputIris);
			outputIris = doubleData[8]; // = precamClient.mStatus.iris;

			BUILDHANDLE(outputSubject);
			outputSubject = doubleData[9]; // = precamClient.mStatus.subject;

			BUILDHANDLE(outputDistortion);
			outputDistortion = doubleData[10]; // = precamClient.mStatus.distortion;
			
			BUILDHANDLE(outputInteraxial);
			outputInteraxial = doubleData[11]; // = precamClient.mStatus.interocular;

			BUILDHANDLE(outputConvergence);
			outputConvergence = doubleData[12]; // = precamClient.mStatus.convergence;

			BUILDHANDLE(outputTCHour);
			outputTCHour = doubleData[13]; // = precamClient.mTimecode.hour;

			BUILDHANDLE(outputTCMinute);
			outputTCMinute = doubleData[14]; // = precamClient.mTimecode.minute;

			BUILDHANDLE(outputTCSecond);
			outputTCSecond = doubleData[15]; // = precamClient.mTimecode.second;

			BUILDHANDLE(outputTCFrame);
			outputTCFrame = doubleData[16]; // = precamClient.mTimecode.frame;

			// Compute derived outputs
			// Note: integrated with packet retrieval since that's when values update

			BUILDINHANDLE(inputHorizontalFilmAperture);
			BUILDHANDLE(outputFocalLength);
			double gateWidthMM = inputHorizontalFilmAperture * 25.4;
			outputFocalLength = CameraStatus::getFocalLength(gateWidthMM, outputHFoV);

			BUILDHANDLE(outputVerticalFilmAperture);
			// calculation based on Previzion's fixed aspect ratio, don't care about units or aperture dimensions
			outputVerticalFilmAperture = CameraStatus::getYFoV(inputHorizontalFilmAperture);

			//

			block.setClean( plug );

			releaseDataStorage(buffer);
			//cerr << "precamNode::compute released, buffer count " << threadDataCount() << endl;

			return ( MS::kSuccess );
		}
		else
		{
			//cerr << "precamNode::compute no data ready\n";
			return ( MS::kFailure );
		}
	}

	cerr << "precamNode::compute unknown parameter " << plug.name() << std::endl;
	return ( MS::kUnknownParameter );
}

MStatus initializePlugin( MObject obj )
{
	MStatus status;
	MFnPlugin plugin(obj, PLUGIN_VENDOR, PLUGIN_VERSION, "Any");

	status = plugin.registerNode( "precamNode", 
		precamNode::id,
		precamNode::creator,
		precamNode::initialize,
		MPxNode::kClientDeviceNode );
	if( !status ) {
		status.perror("failed to registerNode precamNode");
	}

	return status;
}

MStatus uninitializePlugin( MObject obj )
{
	MStatus status;
	MFnPlugin plugin(obj);

	status = plugin.deregisterNode(precamNode::id);
	if( !status ) {
		status.perror("failed to deregisterNode precamNode");
	}

	return status;
}
